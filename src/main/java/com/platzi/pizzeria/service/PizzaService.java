package com.platzi.pizzeria.service;
import com.platzi.pizzeria.persitence.entity.PizzaEntity;
import com.platzi.pizzeria.persitence.repository.PizzaPagSortRepository;
import com.platzi.pizzeria.persitence.repository.PizzaRepository;
import com.platzi.pizzeria.service.dto.UpdatePizzaPriceDto;
import com.platzi.pizzeria.service.exception.EmailApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PizzaService {

    private final PizzaRepository pizzaRepository;
    private final PizzaPagSortRepository pizzaPagSortRepository;
    @Autowired
    public PizzaService(PizzaRepository pizzaRepository,PizzaPagSortRepository pizzaPagSortRepository) {
        this.pizzaRepository=pizzaRepository;
        this.pizzaPagSortRepository=pizzaPagSortRepository;
    }


    public Page<PizzaEntity> getAll(int page,int elemennts){
        Pageable pageRequest = PageRequest.of(page,elemennts);
        return this.pizzaPagSortRepository.findAll(pageRequest);
    }

    public Page<PizzaEntity> getAvailable(int page, int elemennts, String sortBy, String sortDirection){
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection),sortBy);
        Pageable pageRequest = PageRequest.of(page,elemennts,sort);
        return this.pizzaPagSortRepository.findByAvailableTrue(pageRequest);
    }

    public List<PizzaEntity> getVeganNameDesc(){
        return this.pizzaRepository.findAllByVeganTrueOrderByNameDesc();
    }

    public PizzaEntity get(int idPizza){
    return this.pizzaRepository.findById(idPizza).orElse(null);
    }

    public PizzaEntity getByName(String name){
        return this.pizzaRepository.findFirstByAvailableTrueAndNameIgnoreCase(name).orElseThrow(()->new RuntimeException("La pizza no existe"));
    }

    public List<PizzaEntity> getWith(String ingrediente) {
        return this.pizzaRepository.findAllByAvailableTrueAndDescriptionContainsIgnoreCase(ingrediente);
    }

    public List<PizzaEntity> getCheapest(double price) {
        return this.pizzaRepository.findTop3ByAvailableTrueAndPriceLessThanEqualOrderByPriceAsc(price);
    }

    public List<PizzaEntity> getWithout(String ingrediente) {
        return this.pizzaRepository.findAllByAvailableTrueAndDescriptionNotContainsIgnoreCase(ingrediente);
    }
    public int getVeganCountTrue() {
        return this.pizzaRepository.countByVeganTrue();
    }

        public PizzaEntity save(PizzaEntity pizza){
        return this.pizzaRepository.save(pizza);
    }

    public void delete(int idPizza){
        this.pizzaRepository.deleteById(idPizza);
    }
    @Transactional(noRollbackFor = EmailApiException.class)
    public void updatePrice(UpdatePizzaPriceDto dto){
        this.pizzaRepository.updatePrice(dto);
        this.sendEmail();
    }

    private void sendEmail() {
        throw new EmailApiException();
    }


    public Boolean exist(int idPizza){
        return this.pizzaRepository.existsById(idPizza);
    }

}












